﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Thread_handling
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating thread...:");
            Console.WriteLine("Main thread [id: {0}]", Thread.CurrentThread.GetHashCode());
            Thread aThread = new Thread(ThreadMethod);

            aThread.Start();
            Thread.Sleep(100);
            aThread.Name = "new thread";
            Console.ReadLine();
        }


        static void ThreadMethod()
        {
            Console.WriteLine("{0} sorszám: {1}", Thread.CurrentThread.Name, Thread.CurrentThread.GetHashCode());
        }
    }

}
