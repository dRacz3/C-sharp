﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CallbackDelegate
{
    // TODO : fix this... create thread with callback delegate. Did not have time for this.
    class Program
    {
        static void Main(string[] args)
        {
            ThreadWithState tw = ThreadWithState();
        }
    }

    class ThreadWithState
    {
        string boilerplate;
        int number;

        public void ThreadProc(string boilerplate, int num)
        {
            this.boilerplate = boilerplate;
            this.number = num;
        }

        public static void ResultCallback(int lineCount)
        {
            Console.WriteLine("Independent task printed {0} lines", lineCount);
        }
    }
}
