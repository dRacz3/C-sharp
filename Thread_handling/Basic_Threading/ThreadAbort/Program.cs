﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ThreadAbort
{
    class Program
    {
        static void P()
        {
            try
            {
                try
                {
                    try
                    {
                        while (true)
                        {
                            Console.WriteLine("Running..");
                        }
                        ;
                    }
                    catch (ThreadAbortException)
                    {
                        Console.WriteLine("-- inner aborted");
                        Thread.ResetAbort();
                    }
                }
                catch (ThreadAbortException)
                {
                    Console.WriteLine("-- outer aborted");
                    Thread.ResetAbort();
                }
            }
            finally
            {
                Console.WriteLine("Finally..");
            }
        }

        static void Main(string[] args)
        {
            Thread t = new Thread(P);
            t.Start(); Thread.Sleep(1);
            t.Abort();
            t.Join();
            Console.WriteLine("done..");
            Console.ReadLine();
        }
    }
}
