﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace back_foreground_Threads
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t = new Thread(printSomething);
            // if isBackGround == false -> program will not exit until its running...
            // if isBackGround == true  -> program will wait for the thread to stop.
            t.IsBackground = false;
            t.Start();
        }

        static void printSomething()
        {
            while(true)
            {
                Console.Write("XY");
                Thread.Sleep(10);
            }
        }

    }
}
