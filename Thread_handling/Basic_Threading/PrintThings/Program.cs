﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace PrintThings
{
    class Printer
    {
        char ch;
        int sleepTime;

        public Printer(char ch, int sleepTime)
        {
            this.ch = ch;
            this.sleepTime = sleepTime;
        }

        public void Print()
        {
            Console.Write("Printing starts for {0}", this.ch );
            for (int i = 0; i < 100; i++)
            {
                Console.Write(ch);
                Thread.Sleep(sleepTime);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating printers...");
            Printer starPrinter = new Printer('*', 10);
            Printer hashtagPrinter = new Printer('#', 10);

            Console.WriteLine("Creating threads...");
            Thread starPrinterThread = new Thread(starPrinter.Print);
            Thread hashtagPrinterThread = new Thread(hashtagPrinter.Print);
            Console.WriteLine("Starting threads...");
            starPrinterThread.Start();
            hashtagPrinterThread.Start();
            Console.ReadLine();

        }
    }
}
