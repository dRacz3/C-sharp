﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ThreadJob
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread thread = new Thread(ThreadJob);
            thread.Start((object)10);
        }


        static public void ThreadJob(object par)
        {
            char ch = '#';
            Console.Write("Printing starts for {0}", ch);
            for (int i = 0; i < 100; i++)
            {
                Console.Write(ch);
                Thread.Sleep((int)par);
            }
        }
    }
}
