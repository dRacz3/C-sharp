﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace thread_states_example
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t = new Thread(delegate () { while (true) ; });
            printThreadInfo(t);
            t.Name = "Worker";
            t.Priority = ThreadPriority.BelowNormal;
            t.Start();
            Thread.Sleep(1);
            printThreadInfo(t);
            t.Suspend();
            Thread.Sleep(1);
            printThreadInfo(t);
            t.Resume();
            printThreadInfo(t);
            t.Abort();
            Thread.Sleep(1);
            Console.WriteLine("state = {0}", t.ThreadState);
            Console.ReadLine();

        }

        static void printThreadInfo(Thread t)
        {
            Console.WriteLine("name = {0}, priority = {1}, state = {2}", t.Name, t.Priority, t.ThreadState);
        }
    }
}
