﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace ThreadPool_example
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating threads using ThreadPool methods.. Main thread is: {0} ", Thread.CurrentThread.GetHashCode());
            ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadPoolMethod), ConsoleColor Blue);
            Thread.Sleep(150);
            ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadPoolMethod), ConsoleColor Red);
            Thread.Sleep(150);
            ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadPoolMethod), ConsoleColor DarkGreen);
            Thread.Sleep(150);
            ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadPoolMethod), ConsoleColor White);
            Console.ReadLine();
        }

        static void ThreadPoolMethod(object state)
        {
            var textColor = (ConsoleColor)state;
            Console ForeGroundColor = textColor;
            Console.WriteLine("Line num " + Thread.CurrentThread.ManagedThreadId);

            DisplayThreadData();
            DisplayNumbers();

            Console ForeGroundColor = textColor;
            Console.WriteLine = "End";
        }

        static void DisplayNumbers()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Number is : {0}", i);
            }
        }

        static void DisplayThreadData()
        {

            Console.WriteLine("{0} sorszám: {1}", Thread.CurrentThread.Name, Thread.CurrentThread.GetHashCode());
        }
    }
}
