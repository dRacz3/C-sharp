﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace StartProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            Process newProcess = new Process();
            newProcess.StartInfo = new ProcessStartInfo("HelloProcess.exe", "Gyurika");
            newProcess.StartInfo.UseShellExecute = false;
            newProcess.StartInfo.RedirectStandardOutput = true;

            newProcess.Start();
            newProcess.WaitForExit();
            Console.WriteLine(newProcess.StandardOutput.ReadToEnd());

            Console.ReadLine();
        }
    }
}
