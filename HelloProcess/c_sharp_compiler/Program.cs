﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_sharp_compiler
{
    class Program
    {
        static void Main(string[] args)
        {

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = String.Format(@"{0}\..\Microsoft.NET\Framework\v{1}\csc.exe", Environment.GetFolderPath(Environment.SpecialFolder.System), Environment.Version.ToString(3));
            startInfo.Arguments = String.Format(@"/nologo /t:exe {0}", args[0]);
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;

            Process compilerProcess = new Process();
            compilerProcess.StartInfo = startInfo;
            compilerProcess.Start();
            compilerProcess.WaitForExit();
            String result = compilerProcess.StandardOutput.ReadToEnd();
            if (String.IsNullOrEmpty(result))
            {
                Console.WriteLine(String.Format(@"Compilation of {0} was successful", args[0]));
                Process compiledProcess = new Process();
                compiledProcess.StartInfo = new ProcessStartInfo("Program.exe", "Gyurika");
                compiledProcess.StartInfo.UseShellExecute = false;
                compiledProcess.StartInfo.RedirectStandardOutput = true;

                compiledProcess.Start();
                compiledProcess.WaitForExit();
                Console.WriteLine(compiledProcess.StandardOutput.ReadToEnd());
            }
            else
            {
                Console.WriteLine("Compilation failed:");
                Console.WriteLine(result);
            }
            Console.ReadLine();
        }

    }
}
